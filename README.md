# Resistive Power Splitter/Divider

## Documentation

Full documentation for the splitter/divider board is available at 
[RF Blocks](https://rfblocks.org/boards/Resistive-Splitter.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
